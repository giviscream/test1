﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneWayCircularList
{
    interface ICircularLinkedList<T> : IEnumerable<T>
    {
        void Add(T data);

        bool Remove(T data);

        void Clear();
    }
}
